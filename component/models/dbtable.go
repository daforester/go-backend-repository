package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"reflect"
)

type Table interface {
	TableName() string
}

type Dbtable struct {
	gorm.Model
}

func (d Dbtable) TableName() string {
	return "Dbtable"
}

func (d *Dbtable) Stringify(t Table) string {
	s := "&{"

	if reflect.ValueOf(t).IsNil() {
		return "<nil>"
	}

	v := reflect.ValueOf(t).Elem()

	for i := v.NumField() - 1; i >= 0; i-- {
		f := v.Field(i)

		if v.Type().Field(i).Tag.Get("print") == "-" {
			continue
		}

		if f.Kind() == reflect.Ptr {
			if f.IsNil() {
				s += " <nil>"
			} else {
				s += " " + fmt.Sprint(f.Elem().Interface())
			}
		} else {
			s += " " + fmt.Sprint(f.Interface())
		}
	}

	return s + "}"
}
