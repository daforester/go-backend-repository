package repositories

import (
	"bitbucket.org/daforester/go-backend-repository/component/libs/mysql"
	"bitbucket.org/daforester/go-backend-repository/component/models"
	"errors"
	"github.com/jinzhu/gorm"
	"reflect"
)

type MySQLRepository struct {
	db    *mysql.MySQL
	Model models.Table
}

func (r *MySQLRepository) Init(DB ...*mysql.MySQL) error {
	if r.db != nil {
		return nil
	}

	if len(DB) > 0 {
		if len(DB) > 1 {
			return errors.New("only one DB may be specified")
		}
		r.db = DB[0]
		return nil
	}

	db, err := mysql.Default()

	if err != nil {
		return err
	}

	r.db = db

	return nil
}

func (r MySQLRepository) All() (interface{}, error) {
	return r.Find()
}

func (r MySQLRepository) Count(w ...interface{}) (uint, error) {
	e := r.Init()

	if e != nil {
		return 0, e
	}

	m := r.makeModel()

	c := new(uint)

	r.db.Count(m, c, w...)

	return *c, nil
}

func (r MySQLRepository) DB() *mysql.MySQL {
	return r.db
}

func (r MySQLRepository) First(w ...interface{}) (interface{}, error) {
	e := r.Init()

	if e != nil {
		return nil, e
	}

	m := r.makeModel()

	r.db.FetchObject(m, w...)

	f := reflect.ValueOf(m)

	if f.Elem().FieldByName("ID").IsValid() {
		id := f.Elem().FieldByName("ID").Interface().(uint)

		if id == 0 {
			return nil, e
		}
	}

	return m, e
}

func (r MySQLRepository) Find(w ...interface{}) (interface{}, error) {
	return r.Paginate(0, 0, w...)
}

func (r MySQLRepository) Paginate(l int, o int, w ...interface{}) (interface{}, error) {
	e := r.Init()

	if e != nil {
		return nil, e
	}

	m := r.makeSlice()

	if l > 0 {
		r.db.Find(m, w...).Limit(l).Offset(o)
	} else {
		r.db.Find(m, w...)
	}

	return m, e
}

func (r MySQLRepository) Create(m models.Table) error {
	return r.Update(m)
}

func (r MySQLRepository) Update(m models.Table) error {
	e := r.Init()

	if e != nil {
		return e
	}

	db := r.db.Save(m)

	if db.Error != nil {
		return db.Error
	}

	return e
}

func (r MySQLRepository) Delete(m models.Table) error {
	return r.remove(m,false)
}

func (r MySQLRepository) Purge(m models.Table) error {
	return r.remove(m, true)
}

func (r MySQLRepository) remove(m models.Table, purge bool) error {
	var db *gorm.DB

	e := r.Init()

	if e != nil {
		return e
	}

	iface := reflect.New(reflect.TypeOf(m))
	f := iface.Elem().FieldByName("ID")

	if f.IsValid() {
		x := reflect.ValueOf(m)
		if x.FieldByName("ID").Uint() > 0 {

			if purge {
				db = r.db.Purge(m)
			} else {
				db = r.db.Delete(m)
			}

			if db.Error != nil {
				return db.Error
			}

			return e
		}
	}

	model := r.makeModel()

	if purge {
		db = r.db.Purge(model, m)
	} else {
		db = r.db.Delete(model, m)
	}

	if db.Error != nil {
		return db.Error
	}

	return e
}

func (r MySQLRepository) Related(m models.Table, w interface{}, k ...string) {
	r.db.Related(m, w, k...)
}

func (r MySQLRepository) Replace(m models.Table, column string, w interface{}) {
	r.db.Replace(m, column, w)
}

func (r MySQLRepository) makeModel() interface{} {
	t := reflect.New(reflect.ValueOf(r.Model).Elem().Type()).Elem()

	x := reflect.New(t.Type())
	x.Elem().Set(t)

	return x.Interface()
}

func (r MySQLRepository) makeSlice() interface{} {
	m := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(r.Model)), 0, 0)

	x := reflect.New(m.Type())
	x.Elem().Set(m)

	return x.Interface()
}
