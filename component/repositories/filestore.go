package repositories

type FileStore interface {
	Get(string) ([]byte, error)
	Put(string, []byte) error
}
