package repositories

import (
	"bitbucket.org/daforester/go-backend-repository/component/libs/minio"
	"bytes"
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"reflect"
)

type MinioStore struct {
	minio *minio.Minio
}

func (m *MinioStore) Init() error {
	if m.minio != nil {
		return nil
	}

	min, err := minio.Default()

	if err != nil {
		return err
	}

	m.minio = min

	return nil
}

func (m *MinioStore) Get(name string) ([]byte, error) {
	return m.minio.Fetch(name)
}

func (m *MinioStore) List(name string) ([]string, error) {
	return m.minio.List(name)
}

func (m *MinioStore) Put(name string, d interface{}) error {
	switch d.(type) {
	case *multipart.FileHeader:
		return m.PutFromMultipartFileHeader(name, d.(*multipart.FileHeader))
	case []byte:
		return m.PutFromBinary(name, d.([]byte))
	}

	return errors.New("Unsupported interface: " + reflect.TypeOf(d).String())
}

func (m *MinioStore) PutFromMultipartFileHeader(name string, fh *multipart.FileHeader) error {
	data := m.headerToBinary(fh)

	return m.PutFromBinary(name, data)
}

func (m *MinioStore) PutFromBinary(name string, data []byte) error {
	return m.minio.Store(name, data)
}

func (m *MinioStore) Remove(name string) error {
	return m.minio.Remove(name)
}

func (m *MinioStore) FileType(d interface{}) (string, error) {
	switch d.(type) {
	case *multipart.FileHeader:
		data := m.headerToBinary(d.(*multipart.FileHeader))
		return http.DetectContentType(data), nil
	case []byte:
		return http.DetectContentType(d.([]byte)), nil
	}

	return "application/octet-stream", errors.New("Unsupported interface: " + reflect.TypeOf(d).String())
}

func (m *MinioStore) BinaryData(d interface{}) []byte {
	switch d.(type) {
	case *multipart.FileHeader:
		return m.headerToBinary(d.(*multipart.FileHeader))
	case []byte:
		return d.([]byte)
	}

	return nil
}

func (m *MinioStore) headerToBinary(fh *multipart.FileHeader) []byte {
	fo, _ := fh.Open()

	data := bytes.NewBuffer(nil)

	io.Copy(data, fo)

	return data.Bytes()
}
