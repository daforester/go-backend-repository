package repositories

import "bitbucket.org/daforester/go-backend-repository/component/models"

type DBRepository interface {
	All() (interface{}, error)
	First(...interface{}) (interface{}, error)
	Find(w ...interface{}) (interface{}, error)
	Paginate(l int, o int, w ...interface{}) (interface{}, error)
	Create(models.Table) error
	Update(models.Table) error
	Delete(models.Table) error
	Purge(models.Table) error
	Related(models.Table, interface{}, ...string)
	Replace(models.Table, string, interface{})
	Error() error
}
