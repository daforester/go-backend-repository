package minio

import (
	"bitbucket.org/daforester/go-backend-repository/component/libs/strgen"
	"github.com/minio/minio-go"
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"os"
)

type MinioConfig struct {
	Name      string
	Bucket    string
	Location  string
	Endpoint  string
	AccessKey string
	SecretKey string
	UseSSL    bool
}

type Minio struct {
	config MinioConfig

	client *minio.Client
}

var (
	defaultMinio *Minio
	instances    map[string]*Minio
)

func NewMinio(config MinioConfig, opt ...bool) *Minio {
	m := new(Minio)
	m.SetConfig(config)

	if defaultMinio == nil || (len(opt) > 0 && opt[0] == true) {
		defaultMinio = m
	}

	if len(config.Name) > 0 {
		if instances == nil {
			instances = make(map[string]*Minio)
		}

		instances[config.Name] = m
	}

	return m
}

func Default(name ...string) (*Minio, error) {
	if len(name) > 0 && len(name[0]) > 0 {
		m := instances[name[0]]
		if m == nil {
			return nil, errors.New("Minio connection " + name[0] + " not configured")
		}

		return m, nil
	}

	if defaultMinio == nil {
		return defaultMinio, errors.New("Minio connection not configured")
	}

	return defaultMinio, nil
}

func (m *Minio) SetConfig(config MinioConfig) {
	m.config = config
}

func (m *Minio) Connect() error {
	client, err := minio.New(m.config.Endpoint, m.config.AccessKey, m.config.SecretKey, m.config.UseSSL)

	m.client = client

	m.openBucket(m.config.Bucket, m.config.Location)

	return err
}

func (m *Minio) openBucket(bucketName string, location string) error {
	exists, err := m.client.BucketExists(bucketName)

	if err != nil {
		return err
	}

	if !exists {
		err := m.client.MakeBucket(bucketName, location)
		if err != nil {
			return err
		}
	}

	return nil
}

func (m *Minio) Fetch(name string) ([]byte, error) {
	filepath, err := m.tempFilename()

	if err != nil {
		return nil, err
	}

	err = m.client.FGetObject(m.config.Bucket, name, filepath, minio.GetObjectOptions{})

	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadFile(filepath)

	if err != nil {
		return nil, err
	}

	err = os.Remove(filepath)

	return data, err
}

func (m *Minio) List(name string) ([]string, error) {
	doneCh := make(chan struct{})
	defer close(doneCh)

	var files []string

	for message := range m.client.ListObjects(m.config.Bucket, name, false, doneCh) {
		if message.Err != nil {
			return nil, message.Err
		}

		files = append(files, message.Key)
	}

	return files, nil
}

func (m *Minio) Remove(name string) error {
	return m.client.RemoveObject(m.config.Bucket, name)
}

func (m *Minio) Store(name string, filedata []byte) error {
	contentType := http.DetectContentType(filedata)

	filepath, err := m.tempFilename()

	if err != nil {
		return err
	}

	ioutil.WriteFile(filepath, filedata, 0600)
	_, err = m.client.FPutObject(m.config.Bucket, name, filepath, minio.PutObjectOptions{ContentType: contentType})

	if err != nil {
		return err
	}

	err = os.Remove(filepath)

	return err
}

func (m *Minio) tempFilename() (string, error) {
	name, err := strgen.RandString(64)
	if err != nil {
		return "", err
	}

	filepath := os.TempDir() + name

	return filepath, nil
}
