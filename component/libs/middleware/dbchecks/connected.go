package dbchecks

import (
	"bitbucket.org/daforester/go-backend-repository/component/libs/mysql"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"net/http"
)

func IsConnected() gin.HandlerFunc {
	return func(c *gin.Context) {
		db, err := mysql.Default()

		if err != nil {
			logrus.Error(err)
			c.AbortWithStatus(http.StatusServiceUnavailable)
			return
		}

		err = db.DB().DB().Ping()

		if err != nil {
			logrus.Error(err)
			c.AbortWithStatus(http.StatusServiceUnavailable)
		}
	}
}
