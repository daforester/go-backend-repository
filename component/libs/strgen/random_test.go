package strgen

import (
	"reflect"
	"testing"
)

func TestRandString(t *testing.T) {
	x, err := RandString(0)

	if err != nil {
		t.Fatalf("Unexpected error %s", err)
		return
	}

	xType := reflect.TypeOf(x).String()

	if xType != "string" || len(x) == 0 {
		t.Fatalf("Expected string but got %s of size %d", xType, len(x))
	}

	x, err = RandString(16)

	if err != nil {
		t.Fatalf("Unexpected error %s", err)
		return
	}

	xType = reflect.TypeOf(x).String()

	if xType != "string" || len(x) != 16 {
		t.Fatalf("Expected 16 character string but got %s of size %d", xType, len(x))
	}
}

func TestRandBytes(t *testing.T) {
	x, err := RandBytes(0)

	if err != nil {
		t.Fatalf("Unexpected error %s", err)
		return
	}

	xType := reflect.TypeOf(x).String()

	if xType != "[]uint8" || len(x) == 0 {
		t.Fatalf("Expected array of bytes but got %s of size %d", xType, len(x))
	}

	x, err = RandBytes(16)

	if err != nil {
		t.Fatalf("Unexpected error %s", err)
		return
	}

	xType = reflect.TypeOf(x).String()

	if xType != "[]uint8" || len(x) != 16 {
		t.Fatalf("Expected 16 byte long array but got %s of size %d", xType, len(x))
	}
}
