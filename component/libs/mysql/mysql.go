package mysql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pkg/errors"
	"strconv"
	"time"
)

type MySQL struct {
	config MySQLConfig
	db     *gorm.DB
}

type MySQLConfig struct {
	Name    string
	Host    string
	Secret  string
	Port    int64
	Prefix  string
	Preload bool
	Schema  string
	User    string
}

type MySQLResult struct {
	columnNames []string
	rows        *sql.Rows
}

var (
	defaultMySQL *MySQL
	instances    map[string]*MySQL
)

func NewMySQL(config MySQLConfig, opt ...bool) *MySQL {
	m := new(MySQL)
	m.SetConfig(config)

	if defaultMySQL == nil || (len(opt) > 0 && opt[0] == true) {
		defaultMySQL = m
	}

	if len(config.Name) > 0 {
		if instances == nil {
			instances = make(map[string]*MySQL)
		}

		instances[config.Name] = m
	}

	return m
}

func Default(name ...string) (*MySQL, error) {
	if len(name) > 0 && len(name[0]) > 0 {
		m := instances[name[0]]
		if m == nil {
			return nil, errors.New("MySQL Database " + name[0] + " not configured")
		}

		return m, nil
	}

	if defaultMySQL == nil {
		return defaultMySQL, errors.New("MySQL Database not configured")
	}

	return defaultMySQL, nil
}

func (m *MySQL) SetConfig(config MySQLConfig) {
	m.config = config
}

func (m *MySQL) Connect() error {
	dataSourceName := m.config.User + ":" + m.config.Secret + "@tcp(" + m.config.Host + ":" + strconv.FormatInt(m.config.Port, 10) + ")/" + m.config.Schema + "?parseTime=true"
	db, err := gorm.Open("mysql", dataSourceName)

	//db.LogMode(true)

	if err != nil {
		return err
	}

	db.DB().SetMaxIdleConns(0)
	db.DB().SetConnMaxLifetime(10 * time.Second)
	db.DB().SetMaxOpenConns(0)

	db.Set("gorm:auto_preload", m.config.Preload)

	m.db = db

	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return m.config.Prefix + defaultTableName
	}

	return nil
}

func (m *MySQL) Close() error {
	return m.db.Close()
}

func (m *MySQL) buildResult(rows *sql.Rows) (*MySQLResult, error) {

	r := new(MySQLResult)

	r.rows = rows

	c, e := r.rows.Columns()

	if e != nil {
		return nil, e
	}

	r.columnNames = c

	return r, nil
}

func (m *MySQL) Query(q string) (*MySQLResult, error) {
	if m.db == nil {
		return nil, errors.New("No Database Connection")
	}

	e := m.db.DB().Ping()

	if e != nil {
		return nil, e
	}

	rows, e := m.db.DB().Query(q)

	if e != nil {
		return nil, e
	}

	return m.buildResult(rows)
}

func (m *MySQL) ChangeUser(user string, secret string) error {
	m.config.User = user
	m.config.Secret = secret

	e := m.db.Close()

	if e != nil {
		return e
	}

	return m.Connect()
}

func (m *MySQL) DB() *gorm.DB {
	return m.db
}

func (m *MySQL) FetchObject(o interface{}, w ...interface{}) *gorm.DB {
	return m.db.First(o, w...)
}

func (m *MySQL) Find(o interface{}, w ...interface{}) *gorm.DB {
	return m.db.Find(o, w...)
}

func (m *MySQL) Count(o interface{}, c *uint, w ...interface{}) *gorm.DB {
	if len(w) == 0 {
		return m.db.Model(o).Count(c);
	} else {
		return m.db.Model(o).Where(w[0], w[1:]...).Count(c)
	}
}

func (m *MySQL) Save(o interface{}) *gorm.DB {
	return m.db.Save(o)
}

func (m *MySQL) Delete(o interface{}, w ...interface{}) *gorm.DB {
	return m.db.Delete(o, w...)
}

func (m *MySQL) Purge(o interface{}, w ...interface{}) *gorm.DB {
	return m.db.Unscoped().Delete(o, w...)
}

func (m *MySQL) Related(model interface{}, related interface{}, keys ...string) *gorm.DB {
	return m.db.Model(model).Related(related, keys...)
}

func (m *MySQL) Replace(model interface{}, column string, items interface{}) *gorm.Association {
	return m.db.Model(model).Association(column).Replace(items)
}

func (r *MySQLResult) FetchAssoc() (map[string]string, error) {
	if !r.rows.Next() {
		return nil, errors.New("No results remaining")
	}

	rowData := make([]interface{}, len(r.columnNames))
	rowDataPointers := make([]interface{}, len(r.columnNames))

	for i, _ := range r.columnNames {
		rowDataPointers[i] = &rowData[i]
	}

	err := r.rows.Scan(rowDataPointers...)

	if err != nil {
		return nil, err
	}

	row := make(map[string]string)

	for i, colName := range r.columnNames {
		val := rowData[i]
		row[colName] = string(val.([]byte))
	}

	return row, nil
}

func (r *MySQLResult) FetchRow() ([]string, error) {
	if !r.rows.Next() {
		return nil, errors.New("No results remaining")
	}

	rowData := make([]interface{}, len(r.columnNames))
	rowDataPointers := make([]interface{}, len(r.columnNames))

	for i, _ := range r.columnNames {
		rowDataPointers[i] = &rowData[i]
	}

	err := r.rows.Scan(rowDataPointers...)

	if err != nil {
		return nil, err
	}

	row := make([]string, len(r.columnNames))

	for i, _ := range r.columnNames {
		val := rowData[i]
		row[i] = string(val.([]byte))
	}

	return row, nil
}
